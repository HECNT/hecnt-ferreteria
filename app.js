var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var path = require('path');
var exphbs = require('express-handlebars');

app.use(bodyParser.json());
app.use(bodyParser());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.engine('.hbs', exphbs({
	layoutsDir: path.join(__dirname, "client/views/layouts"),
  defaultLayout: 'main',
  extname: 'hbs'
}));

app.set('view engine', '.hbs');
app.set('views', __dirname + '/client/views');


app.use(express.static(__dirname + '/public'));

var navRoute = require("./server/routes/nav");
var homeRoute = require("./server/routes/home");
var loginRoute = require("./server/routes/login");
// var plataformaRoute = require("./server/routes/plataforma");
// var controlRoute = require("./server/routes/control");
// var recepcionRoute = require("./server/routes/recepcion");

app.use('/', navRoute);
app.use('/home', homeRoute);
app.use('/login', loginRoute);
// app.use('/plataforma', plataformaRoute);
// app.use('/control', controlRoute);
// app.use('/recepcion', recepcionRoute);


app.listen(3001, function(){
  console.log('Port : 3001');
});
